﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var bigCasoDuso = '<div class="col-12 col-lg-8 p-4 p-sm-5" style="padding-right: 0 !important;">\
                        <div style="border: 2px solid ##color; padding: 0; margin: 0; height: 100%;">\
                            <article class="card" style="color: white; padding: 0; margin: -18px !important; height: 100%; background-color: ##color">\
                                <div class="card-body pt-5" style="text-align:left">\
                                    <div class="container">\
                                        <div class="row">\
                                            <div class="col-12">\
                                                <div class="container">\
                                                    <div class="row">\
                                                        <div class="col-12">\
                                                            <p class="card-title" style="text-align: left; font-size: 16px; font-weight: 600;    font-family: Titillium Web;"><img src="images/single_block_cat.svg" alt="Alternate Text" /> ##categoria</p>\
                                                        </div>\
                                                    </div>\
                                               </div>\
                                                <div class="container pt-4">\
                                                    <p style="font: var(--unnamed-font-style-normal) normal bold var(--unnamed-font-size-24)/var(--unnamed-line-spacing-36) Titillium Web; text-align: left; font: normal normal bold 24px/36px Titillium Web; letter-spacing: 0.9px; color:white; opacity: 1;">##titolo</p>\
                                                    <p class="text-justify pt-3" style="    font-family: Titillium Web;">##testo</p>\
                                                </div>\
                                            </div>\
                                        </div>\
                                        <div class="row justify-content-center">\
                                            <div class="col-8 d-none d-lg-block">\
                                                <img class="landing-cta-img" src="##mainImage" style="width: 100%;" alt="">\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
 <div class="card-footer" style="text-align: left; padding-left: 35px; padding-bottom: 45px; background-color: ##color;color:white"></div>\
                            </article>\
                        </div >\
                    </div > '
var smallCasoDuso = '<div class="col-12 col-lg-4 p-4 p-sm-5" style="padding-right: 0 !important;">\
                        <div style="border: 2px solid ##color; padding: 0; margin: 0; height: 100%;">\
                            <article class="card" style="color: white; padding: 0; margin: -18px !important; height: 100%; background-color: ##color">\
                                <div class="card-body pt-5" style="text-align:left">\
                                    <div class="container">\
                                        <div class="row">\
                                            <div class="col-12">\
                                                <div class="container">\
                                                    <div class="row">\
                                                        <div class="col-12">\
                                                            <p class="card-title" style="text-align: left; font-size: 16px; font-weight: 600;    font-family: Titillium Web;"><img src="images/single_block_cat.svg" alt="Alternate Text" /> ##categoria</p>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                                <div class="container pt-4">\
                                                    <p style="font: var(--unnamed-font-style-normal) normal bold var(--unnamed-font-size-24)/var(--unnamed-line-spacing-36) Titillium Web; text-align: left; font: normal normal bold 24px/36px Titillium Web; letter-spacing: 0.9px; color:white; opacity: 1;">##titolo</p>\
                                                    <p class="text-justify pt-3" style="    font-family: Titillium Web;">##testo</p>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
 <div class="card-footer" style="text-align: left; padding-left: 35px; padding-bottom: 45px; background-color: ##color;color:white"></div>\
                            </article>\
                        </div>\
                    </div>'
function ValidateEmail(mail) {
    if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail) || mail == "") {
        return (true)
    }

    return (false)
}
function sendMail(el) {
    var check = true
    if ($("#email").val() == "" || !ValidateEmail($("#email").val())) {
        $("#noValidMailMessage").show()
        $("#errorMailMessage").hide()
        $("#successMailMessage").hide()
        $("#mailMessage").modal("show")
        check = false

    }
    if ($("#chisei").val() == "") {
        $("#noValidMailMessage").show()
        $("#errorMailMessage").hide()
        $("#successMailMessage").hide()
        $("#mailMessage").modal("show")
        check = false
    }
    if ($("#testomail").val() == "") {
        $("#noValidMailMessage").show()
        $("#errorMailMessage").hide()
        $("#successMailMessage").hide()
        $("#mailMessage").modal("show")
        check = false
    }
    if (check) {
        var obj = new Object()
        obj.email = $("#email").val()
        obj.chisei = $("#chisei").val()
        obj.testo = $("#testomail").val()
        var formData = new FormData()

        formData.append("obj", JSON.stringify(obj))
        $.ajax({
            url: "/mail/SendMail",
            data: formData,
            processData: false,
            contentType: false,
            type: "POST",
            success: function (retu) {


                if (retu != 0) {
                    $("#errorMailMessage").show()
                    $("#successMailMessage").hide()
                    $("#noValidMailMessage").hide()
                    $("#mailMessage").modal("show")
                } else {
                    $("#errorMailMessage").hide()
                    $("#noValidMailMessage").hide()
                    $("#successMailMessage").show()
                    $("#mailMessage").modal("show")
                }
            }
        });
    }

}

$(document).ready(function () {
    var listaCasiDuso = JSON.parse($("#casiDusoObjCont").html())
    listaCasiDuso.sort((a, b) => (a.posizione > b.posizione) ? 1 : ((b.posizione > a.posizione) ? -1 : 0))
    var index = 0
    $("#casiDusoCont").empty()
    $.each(listaCasiDuso, function () {
        var obj = ''
        var p = this.posizione
        var i = Math.pow(2, p)
        if (p > 0 && p < 5 && (index & i) === 0) {
            if ((p - 1) % 3 === 0) {
                obj = bigCasoDuso
            } else {
                obj = smallCasoDuso
            }

            obj = obj.replace("##categoria", this.categoria)
            obj = obj.replace("##titolo", this.titolo)
            obj = obj.replace("##mainImage", this.mainImage)
            obj = obj.replace("##testo", this.testo)
            obj = obj.replace(/##color/g, this.colore);

            $("#casiDusoCont").append(obj)

            index = index || i
            //index++

        }
    })
});